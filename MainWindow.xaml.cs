﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Practice_30_05
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const string FILE_NAME = "notes.txt";

        MediaPlayer mediaPlayer;
        List<string> notes;
        public MainWindow()
        {
            InitializeComponent();

            notes = new List<string>();
            mediaPlayer = new MediaPlayer();
           
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            
        }


        private void AddNoteButtonClick(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(noteText.Text))
            {
                notes.Add(noteText.Text);

                var thread = new Thread(new ParameterizedThreadStart(WriteFile));
                thread.IsBackground = true;
                thread.Start(new List<string>(notes));

                
                noteText.Text = "";
            }
            else
            {
                MessageBox.Show("Введите текст!");
            }
        }

        private void ChooseButtonClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "MP3 files (*.mp3)|*.mp3|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                mediaPlayer.Open(new Uri(openFileDialog.FileName));

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Start();
        }
        private void WriteFile(object arguments)
        {
            var strings = arguments as List<string>;

            using (StreamWriter file = new StreamWriter(FILE_NAME))
            {
                foreach (var note in strings)
                {
                    file.WriteLine(note);
                }
            }
        }
    }
}
